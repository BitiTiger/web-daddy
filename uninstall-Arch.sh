#!/bin/bash

echo "Acquire sudo privileges..."
sudo -v

echo "Removing config directory..."
sudo rm -rf "/etc/web-daddy"

echo "Remove program directory..."
sudo rm -rf "/usr/local/share/web-daddy"

echo "Removing program link to terminal..."
sudo rm -f "/usr/local/bin/web-daddy"

echo "Configuring systemd..."
sudo systemctl disable web-daddy.service
sudo systemctl stop web-daddy.service

echo "Remove service file for systemd..."
sudo rm -f "/etc/systemd/system/web-daddy.service"

echo "NOTE: Some things require manual deletion."
echo "    Packages can be removed with:"
echo "        sudo pacman -R python nginx"
echo "    The web server directory will need to be deleted."
echo "    The local git repository will need to be deleted."

echo "Done!"
exit