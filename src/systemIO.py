import subprocess
import configEngine
import os


def startNginx():
    """
    This function starts the nginx service.

    Raises:
        ValueError: systemctl returns with an unknown error code
    """
    p = subprocess.run(["systemctl", "start", "nginx.service"])
    code = p.returncode
    if code == 0:
        writeLog("Nginx service started")
    else:
        raise ValueError("Systemctl returned an unknown exit code.")


def stopNginx():
    """
    This function stops the nginx service.

    Raises:
        ValueError: systemctl returns with an unknown exit code
    """
    p = subprocess.run(["systemctl", "stop", "nginx.service"])
    code = p.returncode
    if code == 0:
        writeLog("Nginx service stopped")
    else:
        raise ValueError("Systemctl returns an unknown exit code.")


def checkGitUpdate():
    """
    This function attempts to update the local git repo. If updates are found it will update the web server directory.

    Raises:
        ValueError: git returns with an unknown exit code
    """
    # get needed information
    gitDir = configEngine.getGitDirectory()
    gitURL = configEngine.getGitRemoteURL()
    # get the old commit hash
    p = subprocess.run(["git", "-C", gitDir, "log", "--max-count", "1", "--format=\"%H\""], stdout=subprocess.PIPE)
    code = p.returncode
    oldCommitHash = p.stdout.decode()
    if code == 0:
        writeLog("Got old commit hash %s" % oldCommitHash)
    else:
        raise ValueError("Git log returned an unknown exit code.")
    # try to pull updates from the remote repo
    #p = subprocess.run(["git", "-C", gitDir, "pull", "--strategy=theirs"])
    p = subprocess.run(["git", "-C", gitDir, "pull"])
    code = p.returncode
    if code == 0:
        writeLog("Pulled commit information")
    else:
        raise ValueError("Git pull returned an unknown exit code.")
    # get the new commit hash
    p = subprocess.run(["git", "-C", gitDir, "log", "--max-count", "1", "--format=\"%H\""], stdout=subprocess.PIPE)
    code = p.returncode
    newCommitHash = p.stdout.decode()
    if code == 0:
        writeLog("Got new commit hash %s" % newCommitHash)
    else:
        raise ValueError("Git log returned an unknown exit code.")
    # perform comparison
    if oldCommitHash == newCommitHash:
        writeLog("No new updates were found.")
    else:
        writeLog("Updates were found")
        performUpgrade()


def removeGitRepo():
    """
    This function deletes the local git repository.

    Raises:
        ValueError: rm returns with an unknown exit code
    """
    gitDir = configEngine.getGitDirectory()
    p = subprocess.run(["rm", "-rf", gitDir])
    code = p.returncode
    if code == 0:
        writeLog("Removed git repo from \"%s\"" % gitDir)
    else:
        raise ValueError("File deletion returned an unknown exit code.")


def cloneGitRepo():
    """
    This function clones a remote git repository to the local git directory.

    Raises:
        ValueError: git returns with an unknown exit code
    """
    # get information
    gitDir = configEngine.getGitDirectory()
    gitURL = configEngine.getGitRemoteURL()
    gitBranch = configEngine.getGitBranchName()
    # make git directory
    p = subprocess.run(["mkdir", gitDir])
    code = p.returncode
    if code == 0:
        writeLog("Directory created at %s" % gitDir)
    else:
        raise ValueError("Mkdir returned an unknown exit code.")
    # clone repo
    p = subprocess.run(["git", "clone", gitURL, gitDir])
    code = p.returncode
    if code == 0:
        writeLog("Git repo has been cloned to %s" % gitDir)
    elif code == 128:
        writeLog("The git directory already exists")
    else:
        raise ValueError("Git cloning returned an unknown exit code.")
    # change branch
    p = subprocess.run(["git", "-C", gitDir, "checkout", gitBranch])
    code = p.returncode
    if code == 0:
        writeLog("Branch changed to %s" % gitBranch)
    else:
        raise ValueError("Git checkout returned an unknown exit code.")


def checkGitExists():
    """
    This function ensures that the local git repo exists.
    """
    # get information
    gitDir = configEngine.getGitDirectory()
    if os.path.isdir(gitDir):
        if os.path.isdir(gitDir + "/.git"):
            writeLog("Local git repo exists")
        else:
            # remove files from the directory
            # NOTE: this function deletes the directory since the files are not what is expected
            removeGitRepo()
            # clone the git repo
            cloneGitRepo()
            # update the website
            performUpgrade()
    else:
        # clone the git repo
        cloneGitRepo()
        # update the website
        performUpgrade()


def removeWebsiteData():
    """
    This function removes all files from the web server directory.

    Raises:
        ValueError: rm returns with an unknown exit code
    """
    webDir = configEngine.getWebDirectory()
    p = subprocess.run(["rm", "-rf", webDir + "/*"])
    code = p.returncode
    if code == 0:
        writeLog("Removed website data from \"%s\"" % (webDir))
    else:
        raise ValueError("File deletion returned an unknown exit code.")


def copyWebsiteData():
    """
    This function copies whitelisted files from the local git repo to the web server directory.

    Raises:
        ValueError: mkdir or cp returned with an unknown exit code
    """
    # get needed info
    whitelist = configEngine.getWhitelist()
    webDir = configEngine.getWebDirectory()
    gitDir = configEngine.getGitDirectory()
    # attempt to copy all files
    for filePath in whitelist:
        # split path into required directories and the file
        filePathArray = filePath.split("/")
        # do not make a sub directory if it is not needed
        if (len(filePathArray) > 1):
            # generate the directory path
            directoryPath = ""
            for index in range(len(filePath) - 1):
                directoryPath += filePathArray[index]
                directoryPath += "/"
            # make parent directories
            p = subprocess.run(["mkdir", "-p" + directoryPath])
            code = p.returncode
            if code == 0:
                writeLog("Generated directory path \"%s\"" % directoryPath)
            else:
                raise ValueError("Mkdir returned an unknown error code.")
        # copy file
        sourcePath = gitDir + "/" + filePath
        targetPath = webDir + "/" + filePath
        p = subprocess.run(["cp", "-r", sourcePath, targetPath])
        code = p.returncode
        if code == 0:
            writeLog("Copied file from \"%s\" to \"%s\"" % (sourcePath, targetPath))
        else:
            raise ValueError("cp returned an unknown error code.")
    writeLog("Website data copied successfully")


def performUpgrade():
    """
    This function upgrades the web server directory.
    """
    # NOTE: this expects that the git pull was successful
    # kill the web server
    stopNginx()
    # delete the website
    removeWebsiteData()
    # copy the new data
    copyWebsiteData()
    # start the web server
    startNginx()


def writeLog(text):
    """
    This function prints a string to the terminal in a precise format.

    Args:
        text (str): the text wished to be printed
    """
    print("[System IO] %s." % text)
