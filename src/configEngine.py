import json

CONFIG_PATH = "/etc/web-daddy/web-daddy.conf"

TEMPLATE = {
    "webDirectory": "REPLACEME",
    "gitDirectory": "REPLACEME",
    "gitRemoteURL": "REPLACEME",
    "gitBranchName": "REPLACEME",
    "pollDelay": -1,
    "whitelist": [
        "REPLACEME",
        "REPLACEME",
        "REPLACEME"
    ]
}


def applyTemplate():
    """
    This function writes the default settings into a config file.
    """
    writeLog("Config file is being created")
    data = json.dumps(TEMPLATE, indent=4)
    with open(CONFIG_PATH, "w") as f:
        f.write(data)
    writeLog("Config file has been generated")


def checkFile():
    """
    This function ensures that the config file exists and the settings are valid.

    Raises:
        ValueError: a setting was not changed from the default
        FileNotFoundError: the config file does not exist yet
    """
    try:
        rawData = ""
        with open(CONFIG_PATH, "r") as f:
            rawData = f.read()
        jsonData = json.loads(rawData)
        webDirectory = jsonData['webDirectory']
        if webDirectory == "REPLACEME":
            raise ValueError("You must replace the default config settings with your own.")
        gitDirectory = jsonData['gitDirectory']
        if gitDirectory == "REPLACEME":
            raise ValueError("You must replace the default config settings with your own.")
        gitRemoteURL = jsonData['gitRemoteURL']
        if gitRemoteURL == "REPLACEME":
            raise ValueError("You must replace the default config settings with your own.")
        gitBranchName = jsonData['gitBranchName']
        if gitBranchName == "REPLACEME":
            raise ValueError("You must replace the default config settings with your own.")
        pollDelay = jsonData['pollDelay']
        if pollDelay == -1:
            raise ValueError("You must replace the default config settings with your own.")
        whitelist = jsonData['whitelist']
        for filePath in whitelist:
            if filePath == "REPLACEME":
                raise ValueError("You must replace the default config settings with your own.")
    except FileNotFoundError:
        applyTemplate()
        writeLog("The default settings must be replaced before this program can run")
        exit()


def getWebDirectory():
    """
    This function reads the web directory setting supplied by the config file.

    Returns:
        str: a filesystem path to the web server directory
    """
    checkFile()
    rawData = ""
    with open(CONFIG_PATH, "r") as f:
        rawData = f.read()
    jsonData = json.loads(rawData)
    return jsonData['webDirectory']


def getGitDirectory():
    """
    This function reads the git directory setting supplied by the config file.

    Returns:
        str: a filesystem path to the local git repository
    """
    checkFile()
    rawData = ""
    with open(CONFIG_PATH, "r") as f:
        rawData = f.read()
    jsonData = json.loads(rawData)
    return jsonData['gitDirectory']


def getGitRemoteURL():
    """
    This function reads the git remote url setting supplied by the config file.

    Returns:
        str: a URL to the online git repository
    """
    checkFile()
    rawData = ""
    with open(CONFIG_PATH, "r") as f:
        rawData = f.read()
    jsonData = json.loads(rawData)
    return jsonData['gitRemoteURL']


def getGitBranchName():
    """
    This function reads the git branch setting supplied by the config file.

    Returns:
        str: the name of the git branch to be used by the website
    """
    checkFile()
    rawData = ""
    with open(CONFIG_PATH, "r") as f:
        rawData = f.read()
    jsonData = json.loads(rawData)
    return jsonData['gitBranchName']


def getPollDelay():
    """
    This function reads the poll delay setting supplied by the config file.

    Returns:
        int: the number of seconds to wait before updating the local git repo
    """
    checkFile()
    rawData = ""
    with open(CONFIG_PATH, "r") as f:
        rawData = f.read()
    jsonData = json.loads(rawData)
    return jsonData['pollDelay']


def getWhitelist():
    """
    This function reads the whitelist setting supplied by the config file.

    Returns:
        list[str]: the file paths relative to the git repo that are to be public
    """
    checkFile()
    rawData = ""
    with open(CONFIG_PATH, "r") as f:
        rawData = f.read()
    jsonData = json.loads(rawData)
    return jsonData['whitelist']


def writeLog(text):
    """
    This function prints a string to the terminal in a precise format.

    Args:
        text (str): the text wished to be printed
    """
    print("[Config Engine] %s." % text)
