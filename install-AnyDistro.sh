#!/bin/bash

echo "Acquire sudo privileges..."
sudo -v

echo "Creating config directory..."
sudo mkdir "/etc/web-daddy"

echo "Copying config files..."
sudo cp "./doc/web-daddy.example" "/etc/web-daddy/web-daddy.example"

echo "Creating program directory..."
sudo mkdir "/usr/local/share/web-daddy"

echo "Copying program files..."
sudo cp "./src/main.py" "/usr/local/share/web-daddy/main.py"
sudo cp "./src/systemIO.py" "/usr/local/share/web-daddy/systemIO.py"
sudo cp "./src/integrityCheck.py" "/usr/local/share/web-daddy/integrityCheck.py"
sudo cp "./src/configEngine.py" "/usr/local/share/web-daddy/configEngine.py"
sudo cp "./src/web-daddy" "/usr/local/share/web-daddy/web-daddy"

echo "Changing file permissions..."
sudo chmod +x "/usr/local/share/web-daddy/web-daddy"

echo "Linking program to terminal..."
sudo ln -s "/usr/local/share/web-daddy/web-daddy" "/usr/local/bin/web-daddy"

echo "Copying service file for systemd..."
sudo cp "./src/web-daddy.service" "/etc/systemd/system/web-daddy.service"

echo "Configuring systemd..."
sudo systemctl enable web-daddy.service
sudo systemctl start web-daddy.service

echo "NOTE: Ensure that Python 3.x and Nginx are installed before using this program!"

echo "NOTE: Don't forget to change the default config and restart the web-daddy service."
echo "NOTE: Edit the config file with:"
echo "NOTE:     nano /etc/web-daddy/web-daddy.conf"
echo "NOTE: Restart the web-daddy service with:"
echo "NOTE:     sudo systemctl restart web-daddy.service"

exit
