# Web Daddy

This is a tool for syncing your web server with a remote git repository.

## Installing

- For Ubuntu Linux

    ```sh
    git clone https://gitlab.com/caton101/web-daddy.git
    cd web-daddy
    ./install-Ubuntu.sh
    ```

- For Arch Linux

    ```sh
    git clone https://gitlab.com/caton101/web-daddy.git
    cd web-daddy
    ./install-Arch.sh
    ```

- For any Linux Distro using systemd

    ```sh
    git clone https://gitlab.com/caton101/web-daddy.git
    cd web-daddy
    ./install-AnyDistro.sh
    ```

## Uninstalling

- For Ubuntu Linux

    ```sh
    git clone https://gitlab.com/caton101/web-daddy.git
    cd web-daddy
    ./uninstall-Ubuntu.sh
    ```

- For Arch Linux

    ```sh
    git clone https://gitlab.com/caton101/web-daddy.git
    cd web-daddy
    ./uninstall-Arch.sh
    ```

- For any Linux Distro using systemd

    ```sh
    git clone https://gitlab.com/caton101/web-daddy.git
    cd web-daddy
    ./uninstall-AnyDistro.sh
    ```

## Configuration

**NOTE: The program must run at least once for the config file to be generated.**

The config file can be found at `/etc/web-daddy/web-daddy.conf`. This file is in a JSON format and does not support comments.

This is an example of the config file and a chart explaining each field:

```json
{
    "webDirectory": "REPLACEME",
    "gitDirectory": "REPLACEME",
    "gitRemoteURL": "REPLACEME",
    "gitBranchName": "REPLACEME",
    "pollDelay": -1,
    "whitelist": [
        "REPLACEME",
        "REPLACEME",
        "REPLACEME"
    ]
}
```

| Field         | Description                                                                         |
| :------------ | :---------------------------------------------------------------------------------- |
| webDirectory  | a filesystem path to where the web server files are located                         |
| gitDirectory  | a filesystem path to where the remote git repository will be cloned                 |
| gitRemoteURL  | a remote git repository URL (cloning over SSH is not tested)                        |
| gitBranchName | a git branch name that will be tracked and copied to the web directory              |
| pollDelay     | amount of time in seconds to wait before checking for updates                       |
| whitelist     | a list of files to copy to the web server (paths are relative to the git directory) |
